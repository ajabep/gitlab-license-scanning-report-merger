package main

import (
	"context"
	"encoding/json"
	"os"

	"github.com/qri-io/jsonschema"
)

var jsonSchemaV2 = []byte(`
	{
		"type": "object",
		"properties": {
			"version": {
				"type": "string",
				"minLength": 1,
				"pattern": "[0-9]+(\\.[0-9]+)?"
			},
			"licenses": {
				"type": "array",
				"uniqueItems": true,
				"items": {
					"type": "object",
					"properties": {
						"id": {
							"type": "string",
							"minLength": 1
						},
						"name": {
							"type": "string",
							"minLength": 1
						},
						"url": {
							"type": "string"
						},
						"count": {
							"type": "integer",
							"minimum": 1
						}
					},
					"required": [
					"id",
					"name",
					"url",
					"count"
					]
				}
			},
			"dependencies": {
				"type": "array",
				"uniqueItems": true,
				"items": {
					"type": "object",
					"properties": {
						"name": {
							"type": "string",
							"minLength": 1
						},
						"url": {
							"type": "string"
						},
						"description": {
							"type": "string"
						},
						"paths": {
							"type": "array",
							"items": {
								"type": "string",
								"minLength": 1
							}
						},
						"licenses": {
							"type": "array",
							"items": {
								"type": "string",
								"minLength": 1
							}
						}
					},
					"required": [
					"name",
					"url",
					"description",
					"paths",
					"licenses"
					]
				}
			}
		},
		"required": [
		"version",
		"licenses",
		"dependencies"
		],
		"title": "LicenseReportV2"
	}
`)

var versionStrV2 = "2.0"

// LicenseV2 describe a license
// ex:
//   {
//     "id": "Apache-2.0",
//     "name": "Apache License 2.0",
//     "url": "https://opensource.org/licenses/Apache-2.0",
//     "count": 1
//   }
type LicenseV2 struct {
	id    string
	name  string
	url   string
	count int
}

// Equals compare two LicenseV2
func (l *LicenseV2) Equals(l2 License) bool {
	l2V2, ok := l2.(*LicenseV2)
	if !ok {
		return false
	}

	if l.id != l2V2.id {
		return false
	}

	if l.name != l2V2.name {
		return false
	}

	if l.url != l2V2.url {
		return false
	}

	if l.count != l2V2.count {
		return false
	}

	return true
}

// Import imports value using a map[string]interface{} which come from JSON
func (l *LicenseV2) Import(m map[string]interface{}) {
	l.id = m["id"].(string)
	l.name = m["name"].(string)
	l.url = m["url"].(string)
	l.count = int(m["count"].(float64))
}

// MarshalJSON export a LicenseV2 in JSON
func (l *LicenseV2) MarshalJSON() ([]byte, error) {
	stuff := make(map[string]interface{})
	stuff["id"] = l.id
	stuff["name"] = l.name
	stuff["url"] = l.url
	stuff["count"] = l.count
	return json.Marshal(&stuff)
}

// DependencyV2 describe a dependency
// ex:
//   {
//   	"name": "github.com/google/gofuzz",
//   	"url": "",
//   	"description": "",
//   	"paths": [
//   		"."
//   	],
//   	"licenses": [
//   		"Apache-2.0"
//   	]
//   }
type DependencyV2 struct {
	name        string
	url         string
	description string
	paths       []string
	licenses    []string
}

// Equals compare two DependencyV2
func (d *DependencyV2) Equals(d2 Dependency) bool {
	d2V2, ok := d2.(*DependencyV2)
	if !ok {
		return false
	}

	if d.name != d2V2.name {
		return false
	}

	if d.url != d2V2.url {
		return false
	}

	if d.description != d2V2.description {
		return false
	}

	if len(d.paths) != len(d2V2.paths) {
		return false
	}

	if len(d.licenses) != len(d2V2.licenses) {
		return false
	}

	for _, p := range d.paths {
		if !hasStrings(d2V2.paths, p) {
			return false
		}
	}

	for _, l := range d.licenses {
		if !hasStrings(d2V2.licenses, l) {
			return false
		}
	}

	return true
}

// Import imports value using a map[string]interface{} which come from JSON
func (d *DependencyV2) Import(m map[string]interface{}) {
	d.name = m["name"].(string)
	d.url = m["url"].(string)
	d.description = m["description"].(string)
	d.paths = make([]string, 0)
	d.licenses = make([]string, 0)

	for _, i := range m["paths"].([]interface{}) {
		d.paths = append(d.paths, i.(string))
	}
	for _, i := range m["licenses"].([]interface{}) {
		d.licenses = append(d.licenses, i.(string))
	}
}

// MarshalJSON export a DependencyV2 in JSON
func (d *DependencyV2) MarshalJSON() ([]byte, error) {
	stuff := make(map[string]interface{})
	stuff["name"] = d.name
	stuff["url"] = d.url
	stuff["description"] = d.description
	stuff["paths"] = make([]string, 0)
	stuff["licenses"] = make([]string, 0)

	for _, p := range d.paths {
		stuff["paths"] = append(stuff["paths"].([]string), p)
	}
	for _, l := range d.licenses {
		stuff["licenses"] = append(stuff["licenses"].([]string), l)
	}
	return json.Marshal(&stuff)
}

// LicenseReportV2 represent a report generated by Gitlab CI default job
// ex:
//   {
//   	"version": "2.0",
//   	"licenses": [
//   		{
//   			"id": "Apache-2.0",
//   			"name": "Apache License 2.0",
//   			"url": "https://opensource.org/licenses/Apache-2.0",
//   			"count": 1
//   		},
//   		{
//   			"id": "unknown",
//   			"name": "unknown",
//   			"url": "",
//   			"count": 1
//   		}
//   	],
//   	"dependencies": [
//   		{
//   			"name": "github.com/google/gofuzz",
//   			"url": "",
//   			"description": "",
//   			"paths": [
//   				"."
//   			],
//   			"licenses": [
//   				"Apache-2.0"
//   			]
//   		},
//   		{
//   			"name": "gitlab.com/ajabep/test-ci-go/greetings",
//   			"url": "",
//   			"description": "",
//   			"paths": [
//   				"."
//   			],
//   			"licenses": [
//   				"unknown"
//   			]
//   		}
//   	]
//   }
type LicenseReportV2 struct {
	version      string
	licenses     map[string]LicenseV2
	dependencies map[string]DependencyV2
}

// NewLicenseReportV2 instantiate a LicenseReportV2
func NewLicenseReportV2() LicenseReport {
	lr := LicenseReportV2{
		versionStrV2,
		make(map[string]LicenseV2),
		make(map[string]DependencyV2),
	}
	return &lr
}

// Merge merges 2 LicenseReportV2
func (lr *LicenseReportV2) Merge(src LicenseReport) error {
	srcV2, ok := src.(*LicenseReportV2)
	if !ok {
		return ErrNotSupportedVersionNumber
	}

	if lr.version != versionStrV2 {
		return ErrNotSupportedVersionNumber
	}
	if srcV2.version != versionStrV2 {
		return ErrNotSupportedVersionNumber
	}

	for _, l := range srcV2.licenses {
		if _, isKnown := lr.licenses[l.id]; !isKnown {
			l.count = 0
			lr.licenses[l.id] = l
		}
	}

	for _, d := range srcV2.dependencies {
		if _, isKnown := lr.dependencies[d.name]; !isKnown {
			lr.dependencies[d.name] = d
			for _, l := range d.licenses {
				tmp := lr.licenses[l]
				tmp.count++
				lr.licenses[l] = tmp
			}
		}
	}

	for _, l := range lr.licenses {
		if l.count == 0 { // This should not happen
			delete(lr.licenses, l.id)
		}
	}
	return nil
}

// Equals compare two LicenseReportV2
func (lr *LicenseReportV2) Equals(l2 LicenseReport) bool {
	l2V2, ok := l2.(*LicenseReportV2)
	if !ok {
		return false
	}

	if lr.version != l2V2.version {
		return false
	}

	if len(lr.licenses) != len(l2V2.licenses) {
		return false
	}

	if len(lr.dependencies) != len(l2V2.dependencies) {
		return false
	}

	for id, li1 := range lr.licenses {
		li2, exists := l2V2.licenses[id]
		if !exists || !li1.Equals(&li2) {
			return false
		}
	}

	for id, d := range lr.dependencies {
		d2, exists := l2V2.dependencies[id]
		if !exists || !d.Equals(&d2) {
			return false
		}
	}

	return true
}

// MarshalJSON export a LicenseReportV2 in JSON
func (lr *LicenseReportV2) MarshalJSON() ([]byte, error) {
	stuff := make(map[string]interface{})
	stuff["version"] = lr.version
	stuff["licenses"] = make([]LicenseV2, 0)
	stuff["dependencies"] = make([]DependencyV2, 0)
	for _, l := range lr.licenses {
		stuff["licenses"] = append(stuff["licenses"].([]LicenseV2), l)
	}
	for _, d := range lr.dependencies {
		stuff["dependencies"] = append(stuff["dependencies"].([]DependencyV2), d)
	}
	return json.Marshal(&stuff)
}

// UnmarshalJSON import a LicenseReportV2 from JSON
func (lr *LicenseReportV2) UnmarshalJSON(b []byte) error {
	rs := &jsonschema.Schema{}
	if err := json.Unmarshal(jsonSchemaV2, rs); err != nil {
		return err
	}

	if errs, err := rs.ValidateBytes(context.Background(), b); err != nil {
		return err
	} else if len(errs) > 0 {
		return ErrWrongSchema
	}

	stuff := make(map[string]interface{})
	if err := json.Unmarshal(b, &stuff); err != nil {
		return err
	}
	lr.version = stuff["version"].(string)
	for _, i := range stuff["licenses"].([]interface{}) {
		l := LicenseV2{}
		l.Import(i.(map[string]interface{}))
		lr.licenses[l.id] = l
	}
	for _, i := range stuff["dependencies"].([]interface{}) {
		d := DependencyV2{}
		d.Import(i.(map[string]interface{}))
		lr.dependencies[d.name] = d
	}
	return nil
}

// Import will set value of the LicenseReportv2 from a JSON contained a file
func (lr *LicenseReportV2) Import(filename string) error {
	file, err := os.Open(filename) // #nosec G304
	if err != nil {
		return err
	}

	if err := json.NewDecoder(file).Decode(lr); err != nil {
		return err
	}
	return nil
}
