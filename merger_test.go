package main

import (
	"io/ioutil"
	"testing"
)

func testWorkflow(t *testing.T, testcase []string) {
	src1, err := ioutil.TempFile("", "src1.*.json")
	if err != nil {
		t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
	}
	src2, err := ioutil.TempFile("", "src2.*.json")
	if err != nil {
		t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
	}
	dst, err := ioutil.TempFile("", "dst.*.json")
	if err != nil {
		t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
	}

	_, _ = src1.Write([]byte(testcase[0]))
	_, _ = src2.Write([]byte(testcase[1]))
	_, _ = dst.Write([]byte(testcase[2]))
	dstVersion, err := getLicenseReportVersion(src1.Name())
	if err != nil {
		t.Fatalf("Get version of the first report fails; returns (%T)%v, (%T)%v", dstVersion, dstVersion, err, err)
	}
	licenseReport, _ := NewLicenseReport(dstVersion)

	if report, err := ParseReport(src1.Name()); err != nil {
		t.Fatalf(`ParseReport(%v) fails (returned (%T)%v, (%T)%v)`, src1.Name(), report, report, err, err)
	} else if err := licenseReport.Merge(report); err != nil {
		t.Fatalf(`License.Merge fails (returned (%T)%v)`, err, err)
	}

	if report, err := ParseReport(src2.Name()); err != nil {
		t.Fatalf(`ParseReport(%v) fails (returned (%T)%v, (%T)%v)`, src2.Name(), report, report, err, err)
	} else if err := licenseReport.Merge(report); err != nil {
		t.Fatalf(`License.Merge fails (returned (%T)%v)`, err, err)
	}

	if reportExpected, err := ParseReport(dst.Name()); err != nil {
		t.Fatalf(`ParseReport(%v) = %v, %v ; fails`, dst.Name(), reportExpected, err)
	} else if !licenseReport.Equals(reportExpected) {
		t.Fatalf(`%v.Merge(%v) = %v ; Expected result = %v`, testcase[0], testcase[1], licenseReport, testcase[2])
	}
}
