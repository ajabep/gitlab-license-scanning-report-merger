package main

import (
	"flag"
	"log"
	"os"
	"sort"
)

// hasStrings test if a string x exists in an array of string a
func hasStrings(a []string, x string) bool {
	index := sort.SearchStrings(a, x)
	return index < len(a) && a[index] == x
}

func printUsage() {
	log.Printf("Usage: %s DST SRC...", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	flag.Usage = printUsage
	flag.Parse()
	if flag.NArg() <= 2 {
		flag.Usage()
		os.Exit(2)
	}
	dst := flag.Arg(0)
	dstVersion, err := getLicenseReportVersion(flag.Arg(1))
	if err != nil {
		log.Fatalf("Get version of the first report fails; returns (%T)%v, (%T)%v", dstVersion, dstVersion, err, err)
	}
	dstReport, _ := NewLicenseReport(dstVersion)

	for i := 1; i < flag.NArg(); i++ {
		log.Printf("Merging report in %v (%d/%d)\n", flag.Arg(i), i, flag.NArg()-1)
		report, err := ParseReport(flag.Arg(i))
		if err != nil {
			log.Fatalf("ParseReport(%v) = (%T)%v, (%T)%v", flag.Arg(i), report, report, err, err)
		}
		if err := dstReport.Merge(report); err != nil {
			log.Fatalf("dstReport.Merge(report) = (%T)%v ; for file %v", err, err, flag.Arg(i))
		}
	}
	log.Printf("Merged report is %v\n", dstReport)

	log.Printf("Writing report to %v", dst)
	if err := WriteReport(dst, dstReport); err != nil {
		log.Fatalf("WriteReport(%v, %v) = (%T)%v", dst, dstReport, err, err)
	}
}
