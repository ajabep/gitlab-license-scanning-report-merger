package main

import (
	"io/ioutil"
	"testing"
)

func TestDifferentVersionNumber(t *testing.T) {
	tests := [][]LicenseReport{
		{
			&LicenseReportV2{
				`2.0`,
				map[string]LicenseV2{},
				map[string]DependencyV2{},
			},
			&LicenseReportV2{
				`1.0`,
				map[string]LicenseV2{},
				map[string]DependencyV2{},
			},
		},
		{
			&LicenseReportV2{
				`2.0`,
				map[string]LicenseV2{},
				map[string]DependencyV2{},
			},
			&LicenseReportV2{
				`2.1`,
				map[string]LicenseV2{},
				map[string]DependencyV2{},
			},
		},
		{
			&LicenseReportV2_1{
				`2.1`,
				map[string]LicenseV2_1{},
				map[string]DependencyV2_1{},
			},
			&LicenseReportV2_1{
				`1.0`,
				map[string]LicenseV2_1{},
				map[string]DependencyV2_1{},
			},
		},
		{
			&LicenseReportV2_1{
				`2.1`,
				map[string]LicenseV2_1{},
				map[string]DependencyV2_1{},
			},
			&LicenseReportV2_1{
				`2.1`,
				map[string]LicenseV2_1{},
				map[string]DependencyV2_1{},
			},
		},
		{
			&LicenseReportV2{
				`2.0`,
				map[string]LicenseV2{},
				map[string]DependencyV2{},
			},
			&LicenseReportV2_1{
				`2.1`,
				map[string]LicenseV2_1{},
				map[string]DependencyV2_1{},
			},
		},
	}

	for _, testcase := range tests {
		res := testcase[0]
		err := res.Merge(testcase[1])
		if err != nil && err != ErrNotSupportedVersionNumber {
			t.Fatalf(`%v.Merge(%v) = (%T)%v ; Expected ErrNotSupportedVersionNumber or nil`, testcase[0], testcase[1], err, err)
		}
		if !res.Equals(testcase[0]) {
			t.Fatalf(`%v.Merge(%v) changes dst to %v ; expected no changes`, testcase[0], testcase[1], res)
		}
	}
}

func TestWrongJSONSchema(t *testing.T) {
	tests := []string{
		`{
		  "version": 1.0,
		  "licenses": [],
		  "dependencies": []
		}`,
		`{
		  "version": 2.0,
		  "licenses": [],
		  "dependencies": []
		}`,
		`{
		  "version": 2.1,
		  "licenses": [],
		  "dependencies": []
		}`,
		`{
		  "licenses": [],
		  "dependencies": []
		}`,
	}

	for i, test := range tests {
		src1, err := ioutil.TempFile("", "src1.*.json")
		if err != nil {
			t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
		}
		_, _ = src1.Write([]byte(test))

		if report, err := ParseReport(src1.Name()); err == nil || err != ErrWrongSchema {
			t.Fatalf(`ParseReport(%v) = (%T)%v, (%T)%v ; expected ErrWrongSchema for id = %v`, src1.Name(), report, report, err, err, i)
		}
	}
}

func TestUnsupportedVersionNumber(t *testing.T) {
	tests := []string{
		`{
		  "version": "3.0",
		  "licenses": [],
		  "dependencies": []
		}`,
		`{
		  "version": "2.2",
		  "licenses": [],
		  "dependencies": []
		}`,
		`{
		  "version": "1.0",
		  "licenses": [],
		  "dependencies": []
		}`,
	}

	for i, test := range tests {
		src1, err := ioutil.TempFile("", "src1.*.json")
		if err != nil {
			t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
		}
		_, _ = src1.Write([]byte(test))

		if report, err := ParseReport(src1.Name()); err == nil || err != ErrNotSupportedVersionNumber {
			t.Fatalf(`ParseReport(%v) = (%T)%v, (%T)%v ; expected ErrNotSupportedVersionNumber for id = %v`, src1.Name(), report, report, err, err, i)
		}
	}
}
