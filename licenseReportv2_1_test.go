package main

import (
	"io/ioutil"
	"testing"
)

func TestLicensev2_1Merge(t *testing.T) {
	tests := [][]LicenseReport{
		{ // Testcase
			NewLicenseReportV2_1(),
			NewLicenseReportV2_1(),
			NewLicenseReportV2_1(),
		},
	}

	for i, test := range tests {
		res := test[0]
		if err := res.Merge(test[1]); err != nil {
			t.Fatalf(`License.Merge fails (returned (%T)%v) ; id=%v`, err, err, i)
		}
		if !res.Equals(test[2]) {
			t.Fatalf(`%v.Merge(%v) = %v ; Expected result = %v`, test[0], test[1], res, test[2])
		}
	}
}

func TestParseAndWriteReportV2_1(t *testing.T) {
	tests := map[string]LicenseReport{
		`{
			"version": "2.1",
			"licenses": [
			],
			"dependencies": [
			]
		}`: &LicenseReportV2_1{
			"2.1",
			map[string]LicenseV2_1{},
			map[string]DependencyV2_1{},
		},

		`{
			"version": "2.1",
			"licenses": [
			{
				"id": "Apache-2.0",
				"name": "Apache License 2.0",
				"url": "https://opensource.org/licenses/Apache-2.0"
			}
			],
			"dependencies": [
			{
				"name": "github.com/google/gofuzz",
				"version": "v1.2.0",
				"package_manager": "go",
				"path": "go.sum",
				"licenses": [
				"Apache-2.0"
				]
			}
			]
		}`: &LicenseReportV2_1{
			"2.1",
			map[string]LicenseV2_1{
				"Apache-2.0": {
					"Apache-2.0",
					"Apache License 2.0",
					"https://opensource.org/licenses/Apache-2.0",
				},
			},
			map[string]DependencyV2_1{
				"github.com/google/gofuzz": {
					"github.com/google/gofuzz",
					"v1.2.0",
					"go",
					"go.sum",
					[]string{
						"Apache-2.0",
					},
				},
			},
		},

		`{
			"version": "2.1",
			"licenses": [
				{
					"id": "Apache-2.0",
					"name": "Apache License 2.0",
					"url": "https://opensource.org/licenses/Apache-2.0"
				},
				{
					"id": "unknown",
					"name": "unknown",
					"url": ""
				}
			],
			"dependencies": [
				{
					"name": "github.com/google/gofuzz",
					"version": "v1.2.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
						"Apache-2.0"
					]
				},
				{
					"name": "gitlab.com/ajabep/test-ci-go/greetings",
					"version": "v1.1.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
						"unknown"
					]
				}
			]
		}`: &LicenseReportV2_1{
			"2.1",
			map[string]LicenseV2_1{
				"Apache-2.0": {
					"Apache-2.0",
					"Apache License 2.0",
					"https://opensource.org/licenses/Apache-2.0",
				},
				"unknown": {
					"unknown",
					"unknown",
					"",
				},
			},
			map[string]DependencyV2_1{
				"github.com/google/gofuzz": {
					"github.com/google/gofuzz",
					"v1.2.0",
					"go",
					"go.sum",
					[]string{
						"Apache-2.0",
					},
				},
				"gitlab.com/ajabep/test-ci-go/greetings": {
					"gitlab.com/ajabep/test-ci-go/greetings",
					"v1.1.0",
					"go",
					"go.sum",
					[]string{
						"unknown",
					},
				},
			},
		},

		`{
			"version": "2.1",
			"licenses": [
				{
					"id": "unknown",
					"name": "unknown",
					"url": ""
				}
			],
			"dependencies": [
				{
					"name": "gitlab.com/ajabep/test-ci-go/greetings",
					"version": "v1.1.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
						"unknown"
					]
				}
			]
		}`: &LicenseReportV2_1{
			"2.1",
			map[string]LicenseV2_1{
				"unknown": {
					"unknown",
					"unknown",
					"",
				},
			},
			map[string]DependencyV2_1{
				"gitlab.com/ajabep/test-ci-go/greetings": {
					"gitlab.com/ajabep/test-ci-go/greetings",
					"v1.1.0",
					"go",
					"go.sum",
					[]string{
						"unknown",
					},
				},
			},
		},

		`{
			"version": "2.1",
			"licenses": [
				{
					"id": "Apache-2.0",
					"name": "Apache License 2.0",
					"url": "https://opensource.org/licenses/Apache-2.0"
				}
			],
			"dependencies": [
				{
					"name": "github.com/google/test",
					"version": "v1.0.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
						"Apache-2.0"
					]
				}
			]
		}`: &LicenseReportV2_1{
			"2.1",
			map[string]LicenseV2_1{
				"Apache-2.0": {
					"Apache-2.0",
					"Apache License 2.0",
					"https://opensource.org/licenses/Apache-2.0",
				},
			},
			map[string]DependencyV2_1{
				"github.com/google/test": {
					"github.com/google/test",
					"v1.0.0",
					"go",
					"go.sum",
					[]string{
						"Apache-2.0",
					},
				},
			},
		},

		`{
			"version": "2.1",
			"licenses": [
				{
					"id": "Apache-2.0",
					"name": "Apache License 2.0",
					"url": "https://opensource.org/licenses/Apache-2.0"
				},
				{
					"id": "unknown",
					"name": "unknown",
					"url": ""
				}
			],
			"dependencies": [
				{
					"name": "github.com/google/test",
					"version": "v1.0.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
						"Apache-2.0"
					]
				},
				{
					"name": "github.com/google/gofuzz",
					"version": "v1.2.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
						"Apache-2.0"
					]
				},
				{
					"name": "gitlab.com/ajabep/test-ci-go/greetings",
					"version": "v1.1.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
						"unknown"
					]
				}
			]
		}`: &LicenseReportV2_1{
			"2.1",
			map[string]LicenseV2_1{
				"Apache-2.0": {
					"Apache-2.0",
					"Apache License 2.0",
					"https://opensource.org/licenses/Apache-2.0",
				},
				"unknown": {
					"unknown",
					"unknown",
					"",
				},
			},
			map[string]DependencyV2_1{
				"github.com/google/test": {
					"github.com/google/test",
					"v1.0.0",
					"go",
					"go.sum",
					[]string{
						"Apache-2.0",
					},
				},
				"github.com/google/gofuzz": {
					"github.com/google/gofuzz",
					"v1.2.0",
					"go",
					"go.sum",
					[]string{
						"Apache-2.0",
					},
				},
				"gitlab.com/ajabep/test-ci-go/greetings": {
					"gitlab.com/ajabep/test-ci-go/greetings",
					"v1.1.0",
					"go",
					"go.sum",
					[]string{
						"unknown",
					},
				},
			},
		},
	}

	for srcStr, report := range tests {
		srcFile, err := ioutil.TempFile("", "src1.*.json")
		_, _ = srcFile.Write([]byte(srcStr))
		if err != nil {
			t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
		}

		if parsedReport, err := ParseReport(srcFile.Name()); err != nil {
			t.Fatalf(`ParseReport(%v) fails (returned (%T)%v, (%T)%v) ; src=%v`, srcFile.Name(), parsedReport, parsedReport, err, err, srcStr)
		} else if !report.Equals(parsedReport) {
			t.Fatalf(`ParseReport(%v) = (%T)%v, (%T)%v ; Expected (%T)%v, (<nil>)<nil>`, srcFile.Name(), parsedReport, parsedReport, err, err, report, report)
		}

		dstFile, err := ioutil.TempFile("", "dst1.*.json")
		if err != nil {
			t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
		}
		if err := WriteReport(dstFile.Name(), report); err != nil {
			t.Fatalf(`WriteReport(%v, %v) = (%T)%v ; expected <nil> on %v`, dstFile.Name(), report, err, err, srcStr)
		}

		// Re-parse after, to avoid changes of formating
		if parsedReport, err := ParseReport(dstFile.Name()); err != nil {
			t.Fatalf(`ParseReport(%v) fails (returned (%T)%v, (%T)%v)`, dstFile.Name(), parsedReport, parsedReport, err, err)
		} else if !report.Equals(parsedReport) {
			t.Fatalf(`ParseReport(%v) = (%T)%v, (%T)%v ; Expected (%T)%v, (<nil>)<nil>`, dstFile.Name(), parsedReport, parsedReport, err, err, report, report)
		}
	}
}

func TestWrongJSONSchemaV2_1(t *testing.T) {
	tests := []string{
		`{
		  "version": "2.1",
		  "dependencies": []
		}`,
		`{
		  "version": "2.1",
		  "licenses": []
		}`,
		`{
		  "licenses": [],
		  "dependencies": []
		}`,
		`{
		  "version": 2.1,
		  "licenses": [],
		  "dependencies": []
		}`,
		`{
		  "version": "2.1",
		  "licenses": "",
		  "dependencies": []
		}`,
		`{
		  "version": "2.1",
		  "licenses": null,
		  "dependencies": []
		}`,
		`{
		  "version": "2.1",
		  "licenses": true,
		  "dependencies": []
		}`,
		`{
		  "version": "2.1",
		  "licenses": [],
		  "dependencies": ""
		}`,
		`{
		  "version": "2.1",
		  "licenses": [],
		  "dependencies": null
		}`,
		`{
		  "version": "2.1",
		  "licenses": [],
		  "dependencies": true
		}`,

		// Wrong License schema
		`{
		  "version": "2.1",
		  "licenses": [
			{
				"name": "Apache License 2.0",
				"url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
		  {
			  "name": "github.com/google/gofuzz",
			  "version": "v1.2.0",
			  "package_manager": "go",
			  "path": "go.sum",
			  "licenses": [
			  "Apache-2.0"
			  ]
		  }
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
				"id": "Apache-2.0",
				"url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
		  {
			  "name": "github.com/google/gofuzz",
			  "version": "v1.2.0",
			  "package_manager": "go",
			  "path": "go.sum",
			  "licenses": [
			  "Apache-2.0"
			  ]
		  }
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
				"id": "Apache-2.0",
				"name": "Apache License 2.0"
			}
		  ],
		  "dependencies": [
		  {
			  "name": "github.com/google/gofuzz",
			  "version": "v1.2.0",
			  "package_manager": "go",
			  "path": "go.sum",
			  "licenses": [
			  "Apache-2.0"
			  ]
		  }
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
				"id": 1,
				"name": "Apache License 2.0",
				"url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
		  {
			  "name": "github.com/google/gofuzz",
			  "version": "v1.2.0",
			  "package_manager": "go",
			  "path": "go.sum",
			  "licenses": [
			  "Apache-2.0"
			  ]
		  }
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
				"id": "Apache-2.0",
				"name": 1,
				"url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
		  {
			  "name": "github.com/google/gofuzz",
			  "version": "v1.2.0",
			  "package_manager": "go",
			  "path": "go.sum",
			  "licenses": [
			  "Apache-2.0"
			  ]
		  }
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
				"id": "Apache-2.0",
				"name": "Apache License 2.0",
				"url": 1
			}
		  ],
		  "dependencies": [
		  {
			  "name": "github.com/google/gofuzz",
			  "version": "v1.2.0",
			  "package_manager": "go",
			  "path": "go.sum",
			  "licenses": [
			  "Apache-2.0"
			  ]
		  }
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			2
		  ],
		  "dependencies": [
			{
			  "name": "github.com/google/gofuzz",
			  "url": "",
			  "description": "",
			  "paths": [
				"."
			  ],
			  "licenses": [
				"Apache-2.0"
			  ]
			}
		  ]
		}`,

		// Wrong Dependency schema
		`{
		  "version": "2.1",
		  "licenses": [
			{
			  "id": "Apache-2.0",
			  "name": "Apache License 2.0",
			  "url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
			{
				"version": "v1.2.0",
				"package_manager": "go",
				"path": "go.sum",
				"licenses": [
				"Apache-2.0"
				]
			}
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
			  "id": "Apache-2.0",
			  "name": "Apache License 2.0",
			  "url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
			{
				"name": "github.com/google/gofuzz",
				"package_manager": "go",
				"path": "go.sum",
				"licenses": [
				"Apache-2.0"
				]
			}
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
			  "id": "Apache-2.0",
			  "name": "Apache License 2.0",
			  "url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
			{
				"name": "github.com/google/gofuzz",
				"version": "v1.2.0",
				"path": "go.sum",
				"licenses": [
				"Apache-2.0"
				]
			}
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
			  "id": "Apache-2.0",
			  "name": "Apache License 2.0",
			  "url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
			{
				"name": "github.com/google/gofuzz",
				"version": "v1.2.0",
				"package_manager": "go",
				"licenses": [
				"Apache-2.0"
				]
			}
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
			  "id": "Apache-2.0",
			  "name": "Apache License 2.0",
			  "url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
			{
				"name": "github.com/google/gofuzz",
				"version": "v1.2.0",
				"package_manager": "go",
				"path": "go.sum"
			}
		  ]
		}`,
		`{
		  "version": "2.1",
		  "licenses": [
			{
			  "id": "Apache-2.0",
			  "name": "Apache License 2.0",
			  "url": "https://opensource.org/licenses/Apache-2.0"
			}
		  ],
		  "dependencies": [
			{
				"name": "github.com/google/gofuzz",
				"version": "v1.2.0",
				"package_manager": "go",
				"path": "go.sum",
				"licenses": [
				1
				]
			}
		  ]
		}`,

		// Version 2 declaring a v2.1
		`{
			"version": "2.1",
			"licenses": [
				{
					"id": "Apache-2.0",
					"name": "Apache License 2.0",
					"url": "https://opensource.org/licenses/Apache-2.0",
					"count": 1
				},
				{
					"id": "unknown",
					"name": "unknown",
					"url": "",
					"count": 1
				}
			],
			"dependencies": [
				{
					"name": "github.com/google/gofuzz",
					"url": "",
					"description": "",
					"paths": [
						"."
					],
					"licenses": [
						"Apache-2.0"
					]
				},
				{
					"name": "gitlab.com/ajabep/test-ci-go/greetings",
					"url": "",
					"description": "",
					"paths": [
						"."
					],
					"licenses": [
						"unknown"
					]
				}
			]
		}`,
	}

	for i, test := range tests {
		src1, err := ioutil.TempFile("", "src1.*.json")
		if err != nil {
			t.Fatalf("Cannot create a temp file: (%T)%v", err, err)
		}
		_, _ = src1.Write([]byte(test))

		if report, err := ParseReport(src1.Name()); err == nil || err != ErrWrongSchema {
			t.Fatalf(`ParseReport(%v) = (%T)%v, (%T)%v ; expected ErrWrongSchema for id = %v`, src1.Name(), report, report, err, err, i)
		}
	}
}

func TestWorkflowV2_1(t *testing.T) {
	tests := [][]string{
		// Testcase
		{
			// l1
			`{
			  "version": "2.1",
			  "licenses": [
			  ],
			  "dependencies": [
			  ]
			}`,
			// l2
			`{
			  "version": "2.1",
			  "licenses": [
			  ],
			  "dependencies": [
			  ]
			}`,
			// res
			`{
			  "version": "2.1",
			  "licenses": [
			  ],
			  "dependencies": [
			  ]
			}`,
		},
		// Testcase
		{
			// l1
			`{
				"version": "2.1",
				"licenses": [
				{
					"id": "Apache-2.0",
					"name": "Apache License 2.0",
					"url": "https://opensource.org/licenses/Apache-2.0"
				}
				],
				"dependencies": [
				{
					"name": "github.com/google/gofuzz",
					"version": "v1.2.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
					"Apache-2.0"
					]
				}
				]
			}`,
			// l2
			`{
			  "version": "2.1",
			  "licenses": [
				{
				  "id": "Apache-2.0",
				  "name": "Apache License 2.0",
				  "url": "https://opensource.org/licenses/Apache-2.0"
				}
			  ],
			  "dependencies": [
				{
					"name": "github.com/google/gofuzz",
					"version": "v1.2.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
					"Apache-2.0"
					]
				}
			  ]
			}`,
			// res
			`{
			  "version": "2.1",
			  "licenses": [
				{
				  "id": "Apache-2.0",
				  "name": "Apache License 2.0",
				  "url": "https://opensource.org/licenses/Apache-2.0"
				}
			  ],
			  "dependencies": [
				{
					"name": "github.com/google/gofuzz",
					"version": "v1.2.0",
					"package_manager": "go",
					"path": "go.sum",
					"licenses": [
					"Apache-2.0"
					]
				}
			  ]
			}`,
		},
		// Testcase
		{
			// l1
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
						"Apache-2.0"
						]
					}
				]
			}`,
			// l2
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					},
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
			// res
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					},
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
		},
		// Testcase
		{
			// l1
			`{
				"version": "2.1",
				"licenses": [
				],
				"dependencies": [
				]
			}`,
			// l2
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					},
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
			// res
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					},
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
		},
		// Testcase
		{
			// l1
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					}
				]
			}`,
			// l2
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
			// res
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					},
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
		},
		// Testcase
		{
			// l1
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/test",
						"version": "v1.0.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					}
				]
			}`,
			// l2
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					},
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
			// res
			`{
				"version": "2.1",
				"licenses": [
					{
						"id": "Apache-2.0",
						"name": "Apache License 2.0",
						"url": "https://opensource.org/licenses/Apache-2.0"
					},
					{
						"id": "unknown",
						"name": "unknown",
						"url": ""
					}
				],
				"dependencies": [
					{
						"name": "github.com/google/test",
						"version": "v1.0.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "github.com/google/gofuzz",
						"version": "v1.2.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"Apache-2.0"
						]
					},
					{
						"name": "gitlab.com/ajabep/test-ci-go/greetings",
						"version": "v1.1.0",
						"package_manager": "go",
						"path": "go.sum",
						"licenses": [
							"unknown"
						]
					}
				]
			}`,
		},
	}

	for _, testcase := range tests {
		testWorkflow(t, testcase)
	}
}
