package main

// Dependency describe a dependency
type Dependency interface {
	// Equals compare two Dependency
	Equals(d2 Dependency) bool

	// Import imports value using a map[string]interface{} which come from JSON
	Import(m map[string]interface{})

	// MarshalJSON export a Dependency in JSON
	MarshalJSON() ([]byte, error)
}
