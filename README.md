# Gitlab License Scanning Report Merger

Util to merge License Scanning Reports, because the tool of Gitlab cannot generate one only file for all a projet.

## Install

```
go get gitlab.com/ajabep/gitlab-license-scanning-report-merger
```

It will put the binary into `$GOBIN` directory. Thus, to invoke it if `$GOBIN` is not in your `$PATH`, do:

```
`go env GOBIN`/gitlab-license-scanning-report-merger
```
