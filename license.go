package main

// License describe a license
type License interface {
	// Equals compare two License
	Equals(l2 License) bool

	// Import imports value using a map[string]interface{} which come from JSON
	Import(m map[string]interface{})

	// MarshalJSON export a License in JSON
	MarshalJSON() ([]byte, error)
}
